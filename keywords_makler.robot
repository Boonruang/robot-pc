*** Keywords ***
TC05-1
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-add-offer}
    Wait Until Page Contains    ${add-offer-page}
    Set Selenium Speed    ${speed-test}
    Focus    ${offer-company-text}
    Input Text    ${offer-company-text}    ${company-name}
    Set Selenium Speed    ${speed-test}
    Capture Page Screenshot    ${directory-img}TC05-1.png
    Close Browser

TC05-2
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-add-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${add-offer-page}
    Input Text    ${land-road-text}    ${land-road}
    Input Text    ${land-address-text}    ${land-address}
    click Element    ${contact-date}
    Set Selenium Speed    ${speed-test}
    click Element    ${contact-subdate}
    Focus    //*[@id="agent-phone"]/input
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-assign-survey-officer}
    Click Element    ${click-assign-name}
    Set Selenium Speed    ${speed-test}
    sleep    2s
    Execute javascript  document.getElementById(${btn-save-offer}).click()
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Wait Until Page Contains    ${incompleted-offer}
    Close Browser

TC05-3
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    add offer completed
    Close Browser

TC05-4
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    add offer completed
    Set Selenium Speed    ${speed-test}
    Reload Page
    Set Selenium Speed    ${speed-test}
    Click Link    ${language-xpath}
    Set Selenium Speed    ${speed-test}
    Capture Page Screenshot    ${directory-img}TC05-4.png
    Close Browser

TC05-5
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-btn}
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById(${reject-btn}).click()
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${reject}
    Click Link    ${cancel-reject-btn}
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    Incompleted Offer
    Click Element    ${assign-btn}
    Set Selenium Speed    ${speed-test}
    Select From List    ${assignee-select}    Außendienst User
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-assignee-save}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    Assign Survey Officer: Außendienst User
    Set Selenium Speed    ${speed-test}
    Click Element    ${assign-btn}
    Set Selenium Speed    ${speed-test}
    Select From List    ${assignee-select}    No Assign
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-assignee-save}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC05-6
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-id}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${no-note}
    Set Selenium Speed    ${speed-test}
    Click Link    ${detail-tab}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${no-detail}
    Set Selenium Speed    ${speed-test}
    Click Element    ${activity-tab}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${active-log}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC05-7
    login new tab
    Maximize Browser Window
    Click Link    ${user-btn}
    Click Element    ${edit-user-btn}
    Select From List    ${group-makler-btn}    ${Maklerdatenbank-survey}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${success-save}
    Set Selenium Speed    ${speed-test}
    Select Window    url=${url-home}
    Set Selenium Speed    ${speed-test}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Capture Page Screenshot    ${directory-img}TC05-7.png
    Set Selenium Speed    ${speed-test}
    Select Window
    Click Element    ${edit-user-btn}
    Select From List    ${group-makler-btn}    ${Maklerdatenbank-survey}
    Select From List    ${group-makler-btn}    ${maklerdatenbank-leader}
    Set Selenium Speed    ${speed-test}
    Click Button    ${save-user}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC01-reporter
    open procom
    login reporter
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Page Should Not Contain     ADD OFFER
    Page Should Not Contain     AGENT
    Click Element    id=list0
    Page Should Not Contain     Add Node
    Click Element    id=detail
    Page Should Not Contain     Add Node
    Close Browser

TC01-maneger
    open procom
    login manager
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Page Should Not Contain     ADD OFFER
    Click Element    id=Agent
    Click Element    id=add
    Set Selenium Speed    ${speed-test}
    Input Text    ${agent-company-name}    ${agent-input-company-name}
    Input Text    ${agent-first-name}    ${agent-input-first-name}
    Input Text    ${agent-last-name}    ${agent-input-last-name}
    Input Text    ${agent-road}    1234
    Set Selenium Speed    ${speed-test}
    Click Element    ${save-agen-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC02-maneger
    open procom
    login manager
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Page Should Not Contain     ADD OFFER
    Click Link    ${click-agen}
    Set Selenium Speed    ${speed-test}
    Click Element    ${edit-agen-btn}
    Input Text    ${agent-company-name}    ${agent-input-company-name}
    Input Text    ${agent-first-name}    ${agent-input-first-name}
    Input Text    ${agent-last-name}    ${agent-input-last-name}
    Input Text    ${agent-road}    1234
    Set Selenium Speed    ${speed-test}
    Click Element    ${save-agen-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC03-maneger
    open procom
    login manager
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Page Should Not Contain     ADD OFFER
    Click Link    ${click-agen}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-conf-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${delete-agen-conf-ok-btn}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC01-survey
    open procom
    login survey
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-add-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${add-offer-page}
    Input Text    ${land-road-text}    ${land-road}
    Input Text    ${land-address-text}    ${land-address}
    click Element    ${contact-date}
    Set Selenium Speed    ${speed-test}
    click Element    ${contact-subdate}
    Set Selenium Speed    ${speed-test}
    Focus    //*[@id="agent-phone"]/input
    Click Element    ${click-assign-survey-officer}
    Click Element    ${click-assign-name}
    Set Selenium Speed    ${speed-test}
    sleep    2s
    Execute javascript  document.getElementById(${btn-save-offer}).click()
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Wait Until Page Contains    ${incompleted-offer}
    Close Browser

TC02-survey
    open procom
    login survey
    Maximize Browser Window
    Click Link    ${language-xpath}
    add offer completed
    Close Browser

TC03-survey
    open procom
    login survey
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-btn}
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById(${reject-btn}).click()
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${reject}
    Click Link    ${cancel-reject-btn}
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    Incompleted Offer
    Click Element    ${assign-btn}
    Set Selenium Speed    ${speed-test}
    Select From List    ${assignee-select}    Außendienst User
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-assignee-save}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    Assign Survey Officer: Außendienst User
    Set Selenium Speed    ${speed-test}
    Click Element    ${assign-btn}
    Set Selenium Speed    ${speed-test}
    Select From List    ${assignee-select}    No Assign
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-assignee-save}
    Set Selenium Speed    ${speed-test}
    Close Browser

TC04-survey
    open procom
    login survey
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-id}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-offer-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${no-note}
    Set Selenium Speed    ${speed-test}
    Click Link    ${detail-tab}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${no-detail}
    Set Selenium Speed    ${speed-test}
    Click Element    ${activity-tab}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${active-log}
    Set Selenium Speed    ${speed-test}
    Close Browser