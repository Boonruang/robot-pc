*** Settings ***
Resource          settings.robot
*** Variable ***
${directory-img}    img/
*** Test Cases ***

# Test Open Procom Website
#      TC01-1

# Test login (Normal Case)
#      TC02-1
# Test login (Sort Dashboard)
#      TC02-3
# Test login (Active menu)
#      TC02-4


# Test Makler 1 (Autofill Offer)
#      TC05-1
# Test Makler 2 (Not Complete Case)
#      TC05-2
# Test Makler 3 (Complete Case)
#      TC05-3
# Test Makler 4 (Reload Page After Save data)
#      TC05-4
# Test Makler 5 (Offer single page)
#      TC05-5
# Test Makler 6 (Node Detail)
#      TC05-6


# Test Agent 1 (Add Agent)
#      TC06-1
# Test Agent 2 (Edit Agent)
#      TC06-2
# Test Agent 3 (Delete Agent)
#      TC06-3

# Test Makler (Reporter ADD OFFER)
#      TC01-reporter
Test Makler (Maneger ADD AGENT)
     TC01-maneger
Test Makler (Maneger Edit AGENT)
     TC02-maneger
Test Makler (Maneger Delete AGENT)
     TC03-maneger
# Test Makler (Survey Leader Add Offer Not Complete Case)
#      TC01-survey
# Test Makler (Survey Leader Add Offer Complete Case)
#      TC02-survey
# Test Makler (Survey Leader Reject)
#      TC03-survey
# Test Makler (Survey Leader Note Detail)
#      TC04-survey

# # Test Member (ADD Member Complete Case)
# #      TC01-Admin
# # Test Member (ADD Member Require Field Case)
# #      TC02-Admin
# Test Member (ADD Member Emty Field Case)
#      TC03-Admin
# Test Member (ADD Member Duplicate Email Case)
#      TC04-Admin
# Test Member (ADD Member Not Email Case)
#      TC05-Admin