*** Keywords ***
TC01-Admin
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${user-btn}
    Click Link    //*[@id="main-usr-member"]/div[2]/a
    Input Text    id=firstname-input    Robot
    Set Selenium Speed    ${speed-test}
    Input Text    id=middlename-input    Framework
    Set Selenium Speed    ${speed-test}
    Input Text    id=lastname-input    Test
    Set Selenium Speed    ${speed-test}
    Input Text    id=phone-input    099999999
    Set Selenium Speed    ${speed-test}
    Input Text    id=fax-input    667766
    Input Text    id=title-input    667766
    Click Element    id=group-makler-input
    Set Selenium Speed    ${speed-test}
    Click Element    //*[@id="group-makler-input"]/option[3]
    Set Selenium Speed    ${speed-test}
    Input Text    id=email-input-add    robot2@20scoops.com
    Input Password    id=password-input-add    20scoops.com
    Input Password    id=confirmpassword-input-add    20scoops.com
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById('save-btn').click()
    Wait Until Page Contains    Robot Framework Test
    Close Browser

TC02-Admin
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${user-btn}
    Click Link    //*[@id="main-usr-member"]/div[2]/a
    Input Text    id=firstname-input    Robot
    Set Selenium Speed    ${speed-test}
    Input Text    id=lastname-input    Test
    Set Selenium Speed    ${speed-test}
    Input Text    id=phone-input    099999999
    Set Selenium Speed    ${speed-test}
    Click Element    id=group-makler-input
    Set Selenium Speed    ${speed-test}
    Click Element    //*[@id="group-makler-input"]/option[3]
    Set Selenium Speed    ${speed-test}
    Input Text    id=email-input-add    robot2@20scoops.com
    Input Password    id=password-input-add    20scoops.com
    Input Password    id=confirmpassword-input-add    20scoops.com
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById('save-btn').click()
    Wait Until Page Contains    Robot Framework Test
    Close Browser

TC03-Admin
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${user-btn}
    Click Link    //*[@id="main-usr-member"]/div[2]/a
    Execute javascript  document.getElementById('save-btn').click()
    Wait Until Page Contains    Some fields is invalid
    Close Browser

TC04-Admin
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${user-btn}
    Click Link    //*[@id="main-usr-member"]/div[2]/a
    Input Text    id=firstname-input    Robot
    Set Selenium Speed    ${speed-test}
    Input Text    id=lastname-input    Test
    Set Selenium Speed    ${speed-test}
    Input Text    id=phone-input    099999999
    Set Selenium Speed    ${speed-test}
    Click Element    id=group-makler-input
    Set Selenium Speed    ${speed-test}
    Click Element    //*[@id="group-makler-input"]/option[3]
    Set Selenium Speed    ${speed-test}
    Input Text    id=email-input-add    robot2@20scoops.com
    Input Password    id=password-input-add    20scoops.com
    Input Password    id=confirmpassword-input-add    20scoops.com
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById('save-btn').click()
    Wait Until Page Contains    Email exist
    Close Browser

TC05-Admin
    login admin
    Maximize Browser Window
    Click Link    ${language-xpath}
    Click Link    ${user-btn}
    Click Link    //*[@id="main-usr-member"]/div[2]/a
    Input Text    id=firstname-input    Robot
    Set Selenium Speed    ${speed-test}
    Input Text    id=lastname-input    Test
    Set Selenium Speed    ${speed-test}
    Input Text    id=phone-input    099999999
    Set Selenium Speed    ${speed-test}
    Click Element    id=group-makler-input
    Set Selenium Speed    ${speed-test}
    Click Element    //*[@id="group-makler-input"]/option[3]
    Set Selenium Speed    ${speed-test}
    Input Text    id=email-input-add    robot2
    Input Password    id=password-input-add    20scoops.com
    Input Password    id=confirmpassword-input-add    20scoops.com
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById('save-btn').click()
    Wait Until Page Contains    This email is invalid
    Close Browser