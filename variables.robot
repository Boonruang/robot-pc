*** Variables ***
#host
${url-host}    http://35.156.252.186

#browser login
${browser}    gc
${url-login}   ${url-host}/login

#browser home
${url-home}    ${url-host}/home


#capture page
${directory-img}    img/

#tag menu
${click-add-offer}    //*[@id="app"]/aside/ul[2]/li[2]/a
${click-my-offer}    //*[@id="app"]/aside/ul[2]/li[3]/a
${click-agen}    id=Agent

#tag language
${language-btn}    id=en

#tag login
${email-login}    id=email-login
${password-login}    id=password-login
${login-btn}    id=login-btn

#tag user
${user-btn}    id=usr
${edit-user-btn}    id=edit-001
${group-makler-btn}    id=group-makler-input
${save-user}    id=save-btn
${group-user-btn}    id=group-user-input

#tag mkl
${mak-btn}    id=mkl
${click-offer-btn}    id=list0
${offer-company-text}    //*[@id="agent-company"]/input
${land-road-text}    //*[@id="land-road"]/input
${land-address-text}    //*[@id="land-address"]/input
${land-zipCode-text}    //*[@id="land-zipCode"]/input
${land-no-text}    //*[@id="land-no"]/input
${land-addition-text}    //*[@id="land-addition"]/input
${land-area-text}    //*[@id="land-area"]/div[1]/input
${land-usableArea-text}    //*[@id="land-usableArea"]/div[1]/input
${review-text}    id=review
${contact-date}    contact-date
${contact-subdate}    //*[@id="main-mkl-offer-add"]/section[1]/div/div[2]/div/div[2]/div[2]/div/div[2]/div/span[5]
${agent-company}    //*[@id="agent-company"]/input
${click-agen-company}    //*[@id="agent-company"]/div/div[3]/div
${click-assign-survey-officer}    assignee
${click-assign-name}    //*[@id="assignee"]/optgroup[1]/option[1]
${btn-save-offer}    'save'
${btn-skip-offer}    id=skip
${reject-btn}    'reject'
${cancel-reject-btn}    //*[@id="main-mkl-offer-single-rejected"]/div/div/a
${assign-btn}    id=assigne
${assignee-select}    id=assignee
${btn-assignee-save}    id=submit
${detail-tab}    id=detail
${activity-tab}    id=activity
${click-offer-id}    //*[@id="list4"]/div[6]/div

#tag agent
${agent-company-name}    //*[@id="agent-company"]/input
${agent-first-name}    //*[@id="agent-fname"]/input
${agent-last-name}    //*[@id="agent-lname"]/input
${agent-road}    //*[@id="company-road"]/input
${save-agen-btn}    id=submit
${click-add-agen}    id=add
${edit-agen-btn}    //*[@id="edit"]/i
${delete-agen-btn}    //*[@id="main-mkl-agent"]/div[4]/div/table/tr[2]/td[5]//*[@id="delete"]
${delete-agen-conf-btn}    css=div.swal2-container.swal2-fade.swal2-shown > div > div.swal2-buttonswrapper > button.swal2-confirm.swal2-styled
${delete-agen-conf-ok-btn}    css=body > div.swal2-container.swal2-fade.swal2-shown > div > div.swal2-buttonswrapper > button.swal2-confirm.swal2-styled

#tag language xpath
${language-xpath}    //*[@id="app"]/nav/div[2]/a[1]

#tag logo procom xpath
${click-logo-procom}    //*[@id="app"]/nav/div[1]/a

#tag Element Should Not Be Visible
${btn-verify}    id=verify
${btn-reject}    id=reject
${btn-assign}    id=assigne

#Arguments login
${speed-test}    1
${admin-id}    admin@20scoops.com
${admin-pass}    admin
${admin-name}    admin procom
${member-id}    team@20scoops.com
${member-pass}    qwerty
${member-name}    Sub2 Admin
${reporter-id}    ssss@gmail.com
${reporter-pass}    20scoops
${reporter-name}    Manager
${manager-id}    sss@gmail.com
${manager-pass}    20scoops
${manager-name}    Manager
${survey-id}    champ@gmail.com
${survey-pass}    20scoops
${survey-name}    Manager


#Arguments mkl
${mkl-home-page}    Home
${incompleted-page}    Incompleted Offer
${add-offer-page}    Add Offer
${company-name}    T
${land-road}    TestRobot
${land-address}    1234
${land-zipCode}    22765
${land-city}    Hamburg
${land-no}    00123
${land-addition}    12345
${land-area}    123.124
${land-usableArea}    123.124
${review}    TestRobot
${completed-offer}    Completed Offer
${incompleted-offer}    Incompleted Offer
${reject}    Rejected
${no-note}    No Note present
${no-detail}    No Detail present
${active-log}    www.procominvest.de

#Arguments agent
${agent-page}    Agent
${agent-input-company-name}    TestRobot
${agent-input-first-name}    Test
${agent-input-last-name}    robot

#Arguments main
${main-page}    Welcome Back

#Arguments user
${user-list-page}    Member List
${maklerdatenbank-manager}    5914120a838dcb6c995a4854
${maklerdatenbank-leader}    5911366a2f2d163c6284e63e
${maklerdatenbank-survey}    591134b72f2d163c6284e63c
${usermanage-noaccess}
${usermanage-admin}    591137331d61ab774532d286
${success-save}    Successfully save user information