*** Keywords ***

add offer completed
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Click Link    ${click-add-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${add-offer-page}
    Input Text    ${land-no-text}    ${land-no}
    Input Text    ${land-road-text}    ${land-road}
    Input Text    ${land-address-text}    ${land-address}
    Input Text    ${land-zipCode-text}    ${land-zipCode}
    Set Selenium Speed    ${speed-test}
    Input Text    ${land-addition-text}    ${land-addition}
    Input Text    ${land-area-text}    ${land-area}
    Input Text    ${land-usableArea-text}    ${land-usableArea}
    Input Text    ${review-text}    ${review}
    Set Selenium Speed    ${speed-test}
    click Element    ${contact-date}
    Set Selenium Speed    ${speed-test}
    click Element    ${contact-subdate}
    Set Selenium Speed    ${speed-test}
    Focus    //*[@id="agent-phone"]/input
    Input Text    ${agent-company}    ${company-name}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-agen-company}
    Set Selenium Speed    ${speed-test}
    Click Element    ${click-assign-survey-officer}
    Click Element    ${click-assign-name}
    Set Selenium Speed    ${speed-test}
    Execute javascript  document.getElementById(${btn-save-offer}).click()
    Set Selenium Speed    ${speed-test}
    Click Button    ${btn-skip-offer}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${completed-offer}
    Set Selenium Speed    ${speed-test}
    Reload Page
    Set Selenium Speed    ${speed-test}