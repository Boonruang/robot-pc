Install Robot framework (OSX)
===================

### ขั้นตอนการติดตั้ง Robot framwork
ต้องติดตั้ง Homebrew ก่อนนะครับ สามารถติดตั้งได้จาก
https://brew.sh/index_th.html
หากมี homebrew อยู่แล้ว ก็ข้ามได้เลยนะครับ

1.ให้เราอัพเดต homebrew ก่อนครับ
```sh
$ brew update && brew upgrade
```
2.ติดตั้ง wxPython ผ่าน Homebrew
```sh
$ brew install wxpython
```
3.ติดตั้ง robotframework
```sh
$ sudo pip install robotframework
```
4.ติดตั้ง ChromeDriver เพื่อให้ robotframework ใช้งาน Google Chrome ได้
https://sites.google.com/a/chromium.org/chromedriver/
เมื่อดาวน์โหลดเรียบร้อยแล้ว ให้นำไฟล์ไปไว้ในโฟเดอร์ /user/local/bin
เพียงเท่านี้ก็สามารถใช้งาน Robot framwork ได้แล้วจ้า
___
### Library
1.ติดตั้ง Selenium2library
```sh
$ sudo pip install robotframework-selenium2library
```
2.ติดตั้ง ImageHorizonLibrary
```sh
$ pip install pillow==2.9.0
$ pip install robotframework-imagehorizonlibrary
```
 - https://github.com/Eficode/robotframework-imagehorizonlibrary
 - https://github.com/robotframework/Selenium2Library

___
### วิธีใช้ ​​​Sublime ในการเขียน Robot framework 
1.กด command + shift + p แล้วพิมพ์ install package

2.เลือก Package Control:Install Package

3.จากนั้นพิมพ์ robot framework assistant

4.ติดตั้ง FixMacPath เพิ่มอีก 1 package

เพียงเท่านี้ก็สามารถใช้งานได้แล้วครับ  สามารถรันเทสโดยการกด comand + b 
___
### วิธีใช้ Ride ในการเขียน Robot framework
1.ติดตั้ง ride
```sh
$ sudo pip install robotframework-ride
```
2.Run ride ด้วคำสั่ง
```sh
$ ride.py
```
แหล่งที่มา https://medium.com/@reawpaichunsoi/%E0%B8%A7%E0%B8%B4%E0%B8%98%E0%B8%B5%E0%B8%81%E0%B8%B2%E0%B8%A3%E0%B8%A5%E0%B8%87-ride-%E0%B8%9A%E0%B8%99-macos-%E0%B8%9C%E0%B9%88%E0%B8%B2%E0%B8%99-homebrew-7405ee532acf 