*** Keywords ***
TC02-1
    login admin
    Click Link    ${language-xpath}
    Close Browser

TC02-2
    login new tab
    Close Browser

TC02-3
    login admin
    Capture Page Screenshot    ${directory-img}TC02-3.png
    Close Browser

TC02-4
    login admin
    Click Link    ${language-xpath}
    Click Link    ${mak-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${mkl-home-page}
    Click Element    ${click-logo-procom}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${main-page}
    Click Link    ${user-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${user-list-page}
    Close Browser

login admin
    open procom
    language
    Input Text    ${email-login}    ${admin-id}
    Input Password    ${password-login}    ${admin-pass}
    Click Button    ${login-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${admin-name}

login member
    language
    Input Text    ${email-login}    ${member-id}
    Input Password    ${password-login}    ${member-pass}
    Click Button    ${login-btn}
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${member-name}

login reporter
    language
    Input Text    ${email-login}    ${reporter-id}
    Input Password    ${password-login}    ${reporter-pass}
    Click Button    ${login-btn}

login manager
    language
    Input Text    ${email-login}    ${manager-id}
    Input Password    ${password-login}    ${manager-pass}
    Click Button    ${login-btn}

login survey
    language
    Input Text    ${email-login}    ${survey-id}
    Input Password    ${password-login}    ${survey-pass}
    Click Button    ${login-btn}

login new tab
    login admin
    Click Link    ${language-xpath}
    Set Selenium Speed    ${speed-test}
    Press Combination    KEY.ctrl    t
    Set Selenium Speed    ${speed-test}
    Type With Keys Down    ${url-login}
    Set Selenium Speed    ${speed-test}
    Press Combination    key.enter
    Set Selenium Speed    ${speed-test}
    Select Window    url=${url-login}
    Set Selenium Speed    ${speed-test}
    login member
    Set Selenium Speed    ${speed-test}
    Click Link    ${language-xpath}
    Set Selenium Speed    ${speed-test}
    Select Window
    Wait Until Page Contains    ${admin-name}
    Reload Page
    Set Selenium Speed    ${speed-test}
    Wait Until Page Contains    ${admin-name}